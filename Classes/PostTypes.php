<?php

namespace Q2C_Calculator;

class PostTypes {
	public static function init() {
		add_action( 'init', [ get_called_class(), 'register_post_types' ], 10 );
	}

	public static function register_post_types() {
		self::register_post_type( 'q2c-result', __( 'Q2C Result', Base::$textdomain ), __( 'Q2C Results', Base::$textdomain ), [
			'menu_icon'          => 'dashicons-analytics',
			'supports'           => [ 'title' ],
		] );
	}

	public static function register_post_type( $posttype, $singular_name, $plural_name, $overwrites = [] ) {

		$args = [
			'labels'              => [
				'name'               => $plural_name,
				'singular_name'      => $singular_name,
				'menu_name'          => $plural_name,
				'name_admin_bar'     => $singular_name,
				'add_new'            => sprintf( _x( 'Add %s', 'Post type label', Base::$textdomain ), $singular_name ),
				'add_new_item'       => sprintf( _x( 'Add new %s', 'Post type label', Base::$textdomain ), $singular_name ),
				'new_item'           => sprintf( _x( 'New %s', 'Post type label', Base::$textdomain ), $singular_name ),
				'edit_item'          => sprintf( _x( 'Edit %s', 'Post type label', Base::$textdomain ), $singular_name ),
				'view_item'          => sprintf( _x( 'View %s', 'Post type label', Base::$textdomain ), $singular_name ),
				'all_items'          => sprintf( _x( 'All %s', 'Post type label', Base::$textdomain ), $plural_name ),
				'search_items'       => sprintf( _x( 'Search %s', 'Post type label', Base::$textdomain ), $plural_name ),
				'parent_item_colon'  => sprintf( _x( 'Parent %s:', 'Post type label', Base::$textdomain ), $plural_name ),
				'not_found'          => sprintf( _x( 'No %s found.', 'Post type label', Base::$textdomain ), $plural_name ),
				'not_found_in_trash' => sprintf( _x( 'No %s found in the trash.', 'Post type label', Base::$textdomain ), $plural_name ),
			],
			'public'              => true,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'exclude_from_search' => true,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'show_in_nav_menus'   => true,
			'supports'            => [ 'title', 'thumbnail', 'editor', 'excerpt' ],
			'menu_icon'           => 'dashicons-media-document',
		];
		register_post_type( $posttype, wp_parse_args( $overwrites, $args ) );

	}
}
