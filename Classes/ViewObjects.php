<?php

namespace Q2C_Calculator;

use WP_Post;

class ViewObjects {
	public static function getLandingpageContent( $post ) {
		$thumbnail_id = get_field( 'thumbnail_id', $post );
		$thumbnail    = self::getImageObject( $thumbnail_id );

		$obj = [
			'thumbnail'        => $thumbnail,
			'video_url'        => get_field( 'video_url', $post ),
			'introduction'     => get_field( 'introduction', $post ),
			'highlighted_text' => get_field( 'highlighted_text', $post ),
			'usps'             => get_field( 'usps', $post ),
		];

		return $obj;
	}

	/**
	 * @param string $image_id
	 * @param bool $is_resp_bg_image
	 * @param string $html_size
	 * @param string $src_size
	 * @param array $attr_overrides
	 *
	 * @return array|bool
	 */
	public static function getImageObject( $image_id, $is_resp_bg_image = false, $html_size = 'large', $src_size = 'medium', $attr_overrides = [] ) {
		if ( empty( $image_id ) ) {
			return false;
		}

		$attachment = get_post( $image_id );
		$mime_type  = get_post_mime_type( $image_id );

		if ( $is_resp_bg_image ) {
			$attributes = wp_parse_args( $attr_overrides, [
				'class'            => 'responsivebackgroundimage',
				'data-resp-bg-img' => '',
			] );
		} else {
			$attributes = wp_parse_args( $attr_overrides, [
				'class' => 'img-responsive',
			] );
		}

		if ( $mime_type === 'image/svg+xml' ) {
			$html_size = $src_size = 'full';
		}

		return [
			'id'          => $image_id,
			'html'        => wp_get_attachment_image( $image_id, $html_size, false, $attributes ),
			'src'         => wp_get_attachment_image_src( $image_id, $src_size, false )[0],
			'src_set'     => wp_get_attachment_image_srcset( $image_id, $html_size ),
			'caption'     => is_object( $attachment ) ? $attachment->post_excerpt : '',
			'description' => is_object( $attachment ) ? $attachment->post_content : '',
			'title'       => is_object( $attachment ) ? $attachment->post_title : '',
			'alt'         => get_post_meta( $image_id, '_wp_attachment_image_alt', true ),
		];
	}

	public static function getCalculateAgainObject( WP_Post $post ) {
		$obj = [
			'action_url' => admin_url( 'admin-post.php' ),
			'nonce'      => wp_create_nonce( 'q2c_calculate_again' ),
			'post_id'    => $post->ID,
			'fields'     => FormBuilder::getCalculatorStepOneFields( $post ),
		];

		return $obj;
	}

	public static function getResultPdfObject( $post_id ) {
		$p = get_post( $post_id );

		$first_name   = get_field( 'first_name', $p );
		$last_name    = get_field( 'last_name', $p );
		$full_name = $first_name . ' ' . $last_name;
		$company_name = get_field( 'company_name', $p );
		$generated_on = '' . date( 'F jS, Y' );

		$fte = Calculator::getIncrementalTimeAvailableForSellingInFTE( $p );
		$value_of_saved_time = Calculator::getValueOfSavedTime( $p );
		$automation_potential = Calculator::getAutomationPotentialPercentage( $p, false );
		$additional_selling_hours = Calculator::getAdditionalSellingHours( $p );
		$additional_selling_hours_increase = Calculator::getAdditonalSellingHoursIncrease( $p );
		$increase_in_selling_time = Calculator::getIncreaseInIncrementalTimeAvailableForSelling( $p );

		$images = [
			'contact_button' => 'contact-button.jpg',
			'fte' => 'fte/' . $fte . '.png',
			'automation_potential' => 'automation-potential-gauge/' . $automation_potential . '.png',
			'selling_hours' => 'donut/' . $additional_selling_hours_increase . '.png',
			'caret_up' => 'caret-up.png',
		];

		$obj = [
			'file_name'                  => 'cleverbridge Quote-to-Cart Result - ' . $company_name,
			'full_name'                  => $full_name,
			'company_name'               => $company_name,
			'generated_on'               => $generated_on,
			'fte'                        => $fte,
			'value_of_saved_time'        => $value_of_saved_time,
			'selling_hours'              => $additional_selling_hours,
			'increase_in_selling_time'   => $increase_in_selling_time,
			'images' => $images,
		];

		return $obj;
	}

	public static function getEmailObject( $post_id ) {
		$obj = [
			'first_name'       => get_field( 'first_name', $post_id ),
			'company_name'     => get_field( 'company_name', $post_id ),
			'email_address'    => get_field( 'email_address', $post_id ),
			'download_pdf_url' => get_permalink( $post_id ) . 'download-as-pdf',
		];

		return $obj;
	}

	public static function getResultContent( WP_Post $post ) {
		$result_page_options = get_field( 'q2c_result_page_options', 'option' );

		if ( $result_page_options['video_and_text']['thumbnail_id'] ) {
			$result_page_options['video_and_text']['thumbnail'] = self::getImageObject( $result_page_options['video_and_text']['thumbnail_id'] );
		}

		if ( $result_page_options['testimonial']['image_id'] ) {
			$result_page_options['testimonial']['image'] = self::getImageObject( $result_page_options['testimonial']['image_id'] );
		}

		if ( $result_page_options['testimonial']['logo_id'] ) {
			$result_page_options['testimonial']['logo'] = self::getImageObject( $result_page_options['testimonial']['logo_id'] );
		}

		if ( is_array( $result_page_options['features']['features'] ) && count( $result_page_options['features']['features'] ) ) {
			foreach ( $result_page_options['features']['features'] as $k => $feature ) {
				$result_page_options['features']['features'][ $k ]['icon'] = self::getImageObject( $feature['icon_id'] );
			}
		}

		if ( is_array( $result_page_options['logos']['logos'] ) && count( $result_page_options['logos']['logos'] ) ) {
			foreach ( $result_page_options['logos']['logos'] as $k => $feature ) {
				$result_page_options['logos']['logos'][ $k ] = self::getImageObject( $feature );
			}
		}

		$fte = Calculator::getIncrementalTimeAvailableForSellingInFTE( $post );
		$fte_image = Utilities::assets_file_path( 'images/fte/' . $fte . '.png' );

		$automation_potential = Calculator::getAutomationPotentialPercentage( $post, false );
		$automation_potential_image = Utilities::assets_file_path( 'images/automation-potential-gauge/' . $automation_potential . '.png' );

		$additional_selling_hours = Calculator::getAdditionalSellingHours( $post );
		$additional_selling_hours_increase = Calculator::getAdditonalSellingHoursIncrease( $post );
		$increase_in_selling_time = Calculator::getIncreaseInIncrementalTimeAvailableForSelling( $post );

		$send_email_nonce = wp_create_nonce('q2c_send_email_nonce');
		$post_id = $post->ID;

		$obj = [
			'result_page_options'        => $result_page_options,
			'title'                      => 'What Quote-to-Cart Can Do For You',
			'subtitle'                   => 'Results for one month:',
			'boxes' => [
				[
					'title'  => 'FTE of incremental time available for selling',
					'number' => $fte,
					'text'   => 'FTE',
					'image'  => $fte_image,
				],
				[
					'title'  => 'Value of<br/>saved time',
					'number' => Calculator::getValueOfSavedTime( $post ),
					'text'   => 'per month',
					'image'  => Utilities::assets_file_path( 'images/icon-pig.png' ),
				],
				[
					'title'                  => 'Increase in active<br/>selling time',
					'number'                 => $additional_selling_hours,
					'text'                   => 'hours',
					'image'                  => Utilities::assets_file_path( 'images/icon-clock.png' ),
					'is_large'               => true,
					'large_image'            => Utilities::assets_file_path( 'images/donut/' . $additional_selling_hours_increase . '.png' ),
					'compared_to_last_month' => $increase_in_selling_time . ' from last month'
				]
			],
			'automation_potential_image' => $automation_potential_image,
			'send_email_nonce'           => $send_email_nonce,
			'post_id'                    => $post_id,
		];

		return $obj;
	}

	public static function getQ2CCalculatorObject() {
		$obj = [
			'action_url'      => admin_url( 'admin-post.php' ),
			'nonce'           => wp_create_nonce( 'q2c_form_nonce' ),
			'step_one_fields' => FormBuilder::getCalculatorStepOneFields(),
			'step_two_fields' => FormBuilder::getCalculatorStepTwoFields(),
		];

		return $obj;
	}
}
