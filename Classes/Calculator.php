<?php

namespace Q2C_Calculator;

use WP_Post;

class Calculator {
	public static $percentage_of_effective_selling_time = .34;
	public static $percentage_backoffice_hours = .3;
	public static $hours_per_fte = 160;

	public static function getAutomationPotentialPercentage( WP_Post $post, $formatted = true ) {
		$revenue = get_field( 'average_monthly_revenue', $post );

		$result = self::getIncreaseInIncrementalTimeAvailableForSelling( $post, false ) * 3;
		$result = round( $result / .05 ) * .05;

		if ( $result < 0 ) {
			$result = 0;
		} elseif ( $revenue < 100000 && $result > .25 ) {
			$result = .25; // If revenue is less than 100K max out at 25%
		} elseif ( $result > 1 ) {
			$result = 1; // Up to a limit of 100%
		}

		return $formatted ? round( $result * 100, 1 ) . '%' : $result * 100;
	}

	public static function getIncrementalTimeAvailableForSellingInFTE( WP_Post $post, $formatted = true ) {
		$hours_saved_per_rep = self::getSalesHoursSavedPerRep( $post );
		$inside_sales_reps   = get_field( 'inside_sales_reps', $post );

		$result = ( $hours_saved_per_rep / self::$hours_per_fte ) * $inside_sales_reps;

		if ( $result < 0 ) {
			$result = 0;
		} elseif ( $result > 10 ) {
			$result = 10;
		}

		return $formatted ? round( $result / .5 ) * .5 : $result;
	}

	public static function getValueOfSavedTime( WP_Post $post, $formatted = true ) {
		$average_rep_salary = get_field( 'average_rep_salary', $post );
		$inside_sales_reps = get_field( 'inside_sales_reps', $post );
		$hours_saved = self::getSalesHoursSavedPerRep( $post, false );

		$result = (( $average_rep_salary / 12 ) * ( $hours_saved / self::$hours_per_fte ) * 1.25) * $inside_sales_reps;

		if ( $result < 0 ) {
			$result = 0;
		}

		return $formatted ? '$' . round( $result, 0 ) : $result;
	}

	public static function getSalesHoursSavedPerRep( WP_Post $post, $formatted = true ) {
		$total_saved_hours = self::getSalesHoursSavedPerTeam( $post, false );
		$inside_sales_reps = get_field( 'inside_sales_reps', $post );

		$result = $total_saved_hours / $inside_sales_reps;

		return $formatted ? round( $result, 1 ) : $result;
	}

	public static function getSalesHoursSavedPerTeam( WP_Post $post, $formatted = true ) {
		$transactions_per_month = get_field( 'transactions_per_month', $post );
		$minutes_per_quote      = get_field( 'minutes_per_quote', $post );

		$result = ( $minutes_per_quote - 1 ) * ( $transactions_per_month / 60 );

		return $formatted ? round( $result, 1 ) : $result;
	}

	public static function getIncreaseInIncrementalTimeAvailableForSelling( WP_Post $post, $formatted = true ) {
		$hours_saved_per_rep = self::getSalesHoursSavedPerRep( $post, false );

		$result = $hours_saved_per_rep / 54.4;

		return $formatted ? round( $result * 100, 1 ) . '%' : $result;
	}

	public static function getAdditonalSellingHoursIncrease( WP_Post $post, $formatted = true) {
		$minutes_per_quote      = get_field( 'minutes_per_quote', $post );
		$transactions_per_month = get_field( 'transactions_per_month', $post );
		$inside_sales_reps      = get_field( 'inside_sales_reps', $post );

		$selling_hours_per_rep_before = self::$hours_per_fte * self::$percentage_of_effective_selling_time;
		$selling_hours_per_rep_after  = $selling_hours_per_rep_before + ( $minutes_per_quote - 1 ) * ( $transactions_per_month / $inside_sales_reps ) / 60;

		$result = 1 - $selling_hours_per_rep_before / $selling_hours_per_rep_after;

		if ( $result < 0 ) {
			$result = 0;
		} elseif ( $result > 1 ) {
			$result = 1;
		}

		return $formatted ? ( round( $result / .05 ) * .05 ) * 100 : $result;
	}

	public static function getAdditionalSellingHours( WP_Post $post, $formatted = true ) {
		$minutes_per_quote      = get_field( 'minutes_per_quote', $post );
		$transactions_per_month = get_field( 'transactions_per_month', $post );
		$inside_sales_reps      = get_field( 'inside_sales_reps', $post );

		$selling_hours_per_rep_before = self::$hours_per_fte * self::$percentage_of_effective_selling_time;
		$selling_hours_per_rep_after  = $selling_hours_per_rep_before + ( $minutes_per_quote - 1 ) * ( $transactions_per_month / $inside_sales_reps ) / 60;

		$result = ( $selling_hours_per_rep_after - $selling_hours_per_rep_before ) * $inside_sales_reps;

		return $formatted ? round( $result, 1 ) : $result;
	}

	// Below is deprecated

	public static function getBackOfficeHoursSaved( WP_Post $post, $formatted = true ) {
		$total_hours_saved = self::getSalesHoursSavedPerTeam( $post );

		$result = $total_hours_saved * self::$percentage_backoffice_hours;

		return $formatted ? round( $result, 1 ) : $result;
	}

	public static function revenueUpliftInUsd( WP_Post $post, $formatted = true ) {
		// TODO
		return '$XXX';
	}

	public static function revenueUpliftInPercentage( WP_Post $post, $formatted = true ) {
		// TODO
		return 'XX%';
	}

}