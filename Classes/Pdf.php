<?php

namespace Q2C_Calculator;

use setasign\Fpdi\PdfParser\StreamReader;

class Pdf {

	public static function init() {
		add_action( 'init', [ get_called_class(), 'add_download_as_pdf_endpoint' ] );
		add_filter( 'request', [ get_called_class(), 'set_download_as_pdf_var' ] );
		add_action( 'template_redirect', [ get_called_class(), 'download_report_as_pdf' ] );
	}

	public static function add_download_as_pdf_endpoint() {
		add_rewrite_endpoint( 'download-as-pdf', EP_PERMALINK );
	}

	public static function download_report_as_pdf() {
		if ( is_singular( 'q2c-result' ) && get_query_var( 'download-as-pdf' ) ) {
			$post_id = get_the_ID();
			$data    = ViewObjects::getResultPdfObject( $post_id );

			$template_page_path = __DIR__ . '/../assets/dist/pdfs/q2c-result-template.pdf';
			$template_page_file = file_get_contents( $template_page_path, 'rb' );

			$defaultConfig = ( new \Mpdf\Config\ConfigVariables() )->getDefaults();
			$fontDirs      = $defaultConfig['fontDir'];

			$defaultFontConfig = ( new \Mpdf\Config\FontVariables() )->getDefaults();
			$fontData          = $defaultFontConfig['fontdata'];

			$mpdf = new \Mpdf\Mpdf( [
				'fontDir'      => array_merge( $fontDirs, [
					__DIR__ . '/../assets/dist/fonts',
				] ),
				'fontdata'     => $fontData + [
						'bariol' => [
							'R' => 'bariol_regular.ttf',
						],
						'roboto' => [
							'R' => 'roboto_regular.ttf',
						]
					],
				'default_font' => 'roboto'
			] );

			$mpdf->SetTitle( $data['file_name'] );
			$mpdf->showImageErrors = true;

			foreach ($data['images'] as $k => $image) {
				$mpdf->imageVars[$k] = file_get_contents( __DIR__ . '/../assets/dist/images/' . $image );
			}

			$template_page_pdf = $mpdf->SetSourceFile( StreamReader::createByString( $template_page_file ) );
			$imported_page_pdf = $mpdf->ImportPage( $template_page_pdf );

			$mpdf->UseTemplate( $imported_page_pdf );
			$mpdf->WriteFixedPosHTML( '<div style="font-size: 12px">' . $data['full_name'] . '</div>', 59.5, 15, 50, 90, 'auto' );
			$mpdf->WriteFixedPosHTML( '<div style="font-size: 12px">' . $data['company_name'] . '</div>', 108.5, 15, 100, 90, 'auto' );
			$mpdf->WriteFixedPosHTML( '<div style="font-size: 11px;color:#B0B0B0;text-align: right;">' . $data['generated_on'] . '</div>', 130, 8, 70, 90, 'auto' );
			$mpdf->WriteFixedPosHTML( '<div style="color: #FF8A00;font-family:\'bariol\', sans-serif;font-weight: bold;font-size:32px;">' . $data['company_name'] . '</div>', 11, 37, 100, 90, 'auto' );

			// Left
			$mpdf->Image( 'var:contact_button', 10, 82, 0, 13, 'jpg', 'https://www.cleverbridge.com/corporate/contact-sales/', true, false );
			$mpdf->WriteFixedPosHTML( '<div style="font-family:\'bariol\', sans-serif;color: #FF8A00;text-align:center;font-size:45px;">' . $data['fte'] . '</div>', 36, 182, 41, 100, 'auto' );
			$mpdf->Image( 'var:fte', 40, 169, 0, 14, 'png', '', true, false );
			$mpdf->WriteFixedPosHTML( '<div style="font-family:\'bariol\', sans-serif;color: #FF8A00;text-align:center;font-size:45px;">' . $data['value_of_saved_time'] . '</div>', 36, 254, 41, 100, 'auto' );

			// Right
			$mpdf->Image( 'var:automation_potential', 115, 47, 0, 45, 'png', '', true, false );
			$mpdf->Image( 'var:selling_hours', 123, 170, 0, 60, 'png', '', true, false );
			$mpdf->WriteFixedPosHTML( '<div style="font-family:\'bariol\', sans-serif;color: #FF8A00;text-align:center;font-size:45px;">' . $data['selling_hours'] . '</div>', 133, 248, 41, 100, 'auto' );

			$mpdf->Image( 'var:caret_up', 142, 196.5, 0, 3.5, 'png', '', true, false );
			$mpdf->WriteFixedPosHTML( '<div style="color: #FF8A00;text-align:center;font-size:22px;">' . $data['increase_in_selling_time'] . '</div>', 135, 193, 41, 100, 'auto' );
			$mpdf->WriteFixedPosHTML( '<div style="font-size: 10px;color:#878787;text-align:center;">from last month</div>', 133, 201, 41, 100, 'auto' );

			$mpdf->Output( $data['file_name'] . '.pdf', 'I' );
		}
	}

	public static function set_download_as_pdf_var( $vars ) {
		isset( $vars['download-as-pdf'] ) and $vars['download-as-pdf'] = true;

		return $vars;
	}
}