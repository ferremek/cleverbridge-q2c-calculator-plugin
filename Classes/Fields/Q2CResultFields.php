<?php

namespace Q2C_Calculator\Fields;

class Q2CResultFields extends Base {
	public static function make() {
		$group = self::setupGroup( 'Q2C Result Options', 1 );

		$group
			->addTab( 'Calculator Data', [ 'placement' => 'left' ] )
				->addFields( Modules::dollar( 'average_monthly_revenue', 'Average Monthly Revenue' ) )
				->addFields( Modules::number( 'transactions_per_month', 'Transactions Per Month' ) )
				->addFields( Modules::dollar( 'average_order_value', 'Average Order Value' ) )
				->addRange( 'renewal_percentage', [ 'append' => '%' ] )
				->addFields( Modules::number( 'inside_sales_reps', 'Inside Sales Reps' ) )
				->addFields( Modules::dollar( 'average_rep_salary', 'Average Rep Salary' ) )
				->addFields( Modules::number( 'minutes_per_quote', 'Minutes Per Quote' ) )
			->addTab( 'Customer Data' )
				->addFields( Modules::text( 'first_name', 'First Name' ) )
				->addFields( Modules::text( 'last_name', 'Last Name' ) )
				->addEmail( 'email_address' )
				->addFields( Modules::text( 'company_name', 'Company Name' ) )
				->addSelect( 'job_role', [
					'choices' => [
						'c_level_executive'       => 'C-Level Executive',
						'director_senior_manager' => 'Director/Senior Manager',
						'departmental_manager'    => 'Departmental Manager',
						'associate'               => 'Associate',
						'technical'               => 'Technical',
						'consultant'              => 'Consultant',
						'other'                   => 'Other'
					]
				] )
				->addTrueFalse('newsletter_subscribe', [ 'ui' => 1 ])

			->setLocation( 'post_type', '==', 'q2c-result' );

		self::createGroup( $group );
	}
}
