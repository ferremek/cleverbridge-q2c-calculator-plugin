<?php

namespace Q2C_Calculator\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;

class Q2CResultPageFields extends Base {
	public static function make() {
		$field_group_title = 'Q2C Result Page Options';

		$group = new FieldsBuilder( sanitize_title_with_dashes( $field_group_title ), [
			'title'      => $field_group_title,
			'menu_order' => 1,
		] );

		$group
			->addGroup( 'q2c_result_page_options', [
				'label' => $field_group_title
			] )

			// Video And Text
			->addTab( 'video_and_text', [
				'placement' => 'left',
			] )
				->addGroup( 'video_and_text' )
					->addUrl( 'video_url', [
						'instructions' => 'Insert a YouTube video url.'
					] )
					->addFields( Modules::image( 'thumbnail_id', 'Video Thumbnail' ) )
					->addFields( Modules::title() )
					->addFields( Modules::textarea() )
				->endGroup()

			// Testimonial
			->addTab( 'testimonial', [
				'placement' => 'left',
			] )
			->addGroup( 'testimonial' )
				->addFields( Modules::textarea() )
				->addFields( Modules::text( 'name', 'Name' ) )
				->addFields( Modules::text( 'company_name', 'Company Name' ) )
				->addFields( Modules::image( 'image_id', 'Image', '', 500, 'jpg' ) )
				->addFields( Modules::image( 'logo_id', 'Logo', '', 300, 'png/svg' ) )
			->endGroup()

			// CTA
			->addTab( 'cta', [
				'label' => 'CTA',
				'placement' => 'left',
			] )
			->addGroup( 'cta', [
				'label' => 'CTA'
			] )
				->addFields( Modules::text() )
				->addFields( Modules::link() )
			->endGroup()

			// Features
			->addTab( 'features', [
				'placement' => 'left',
			] )
			->addGroup( 'features' )
				->addFields( Modules::text( 'topline', 'Topline' ) )
				->addFields( Modules::title() )
				->addFields( Modules::textarea() )
				->addFields( Modules::link( 'button', 'Button' ) )
				->addRepeater( 'features' )
					->addFields( Modules::image( 'icon_id', 'Icon' ) )
					->addFields( Modules::title() )
				->endRepeater()
			->endGroup()

			// Logos
			->addTab( 'logos', [
				'placement' => 'left',
			] )
			->addGroup( 'logos' )
				->addFields( Modules::title() )
				->addGallery( 'logos', [
					'return_format' => 'id',
				] )
			->endGroup()

			->setLocation( 'options_page', '==', 'q2c-result-page-options' );

		self::createGroup( $group );
	}
}
