<?php

namespace Q2C_Calculator\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;

class Base {
    public static $default_image_args = [
        'preview_size'  => 'medium',
        'label'         => 'Image',
        'return_format' => 'id',
    ];

    public static function createGroup( $group ) {
        add_action( 'acf/init', function () use ( $group ) {
            acf_add_local_field_group( $group->build() );
        } );
    }

    public static function setupGroup( $title, $menuOrder = 0, $args = [] ) {
        return new FieldsBuilder( sanitize_title_with_dashes( $title ), wp_parse_args( $args, [
            'title'                 => $title,
            'position'              => 'acf_after_title',
            'style'                 => 'default',
            'label_placement'       => 'top',
            'instruction_placement' => 'label',
            'menu_order'            => $menuOrder,
        ] ) );
    }

    public static function getImageInstructions( $custom_message = '', $img_width = 1024, $type = 'jpg/png/svg', $show_requirements = true ) {
        $requirements = sprintf( 'Minimum requirements: a %s image of %s pixels wide.', $type, $img_width );

        if ( ! empty( $custom_message ) && $show_requirements ) {
            $return = $custom_message . ' ' . $requirements;
        } else if ( ! empty( $custom_message ) && $show_requirements = false ) {
            $return = $custom_message;
        } else {
            $return = $requirements;
        }

        return $return;
    }
}
