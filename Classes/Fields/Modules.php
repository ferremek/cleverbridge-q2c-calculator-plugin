<?php

namespace Q2C_Calculator\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;

class Modules {

    public static function title( $field_name = 'title', $label = 'Title', $args = [] ) {
        $field = new FieldsBuilder( sanitize_title_with_dashes( $field_name ) );
        $field->addText( $field_name, array_merge( [
            'label' => $label,
        ], $args ) );

        return $field;
    }

    public static function title_multi_line( $field_name = 'title', $label = 'Title', $args = [] ) {
        $field = new FieldsBuilder( sanitize_title_with_dashes( $field_name ) );
        $field->addTextarea( $field_name, array_merge( [
            'label'        => $label,
            'rows'         => 3,
            'new_lines'    => 'br',
            'instructions' => 'Line breaks will be saved',
        ], $args ) );

        return $field;
    }

    public static function text( $name = 'text', $label = 'Text', $args = [] ) {
        $field = new FieldsBuilder( sanitize_title_with_dashes( $name ) );
        $field->addText( $name, array_merge( [
            'label' => $label,
        ], $args ) );

        return $field;
    }

    public static function number( $name = 'text', $label = 'Text', $args = [] ) {
        $field = new FieldsBuilder( sanitize_title_with_dashes( $name ) );
        $field->addNumber( $name, array_merge( [
            'label' => $label,
        ], $args ) );

        return $field;
    }

	public static function dollar( $name = 'amount', $label = 'Amount', $args = [] ) {
		$field = new FieldsBuilder( sanitize_title_with_dashes( $name ) );
		$field->addNumber( $name, array_merge( [
			'label' => $label,
			'prepend' => '$',
		], $args ) );

		return $field;
	}

    public static function textarea( $name = 'text', $label = 'Text', $rows = 5, $new_lines = 'wpautop', $args = [] ) {
        $field = new FieldsBuilder( sanitize_title_with_dashes( $name ) );
        $field->addTextarea( $name, array_merge( [
            'label'        => $label,
            'rows'         => $rows,
            'new_lines'    => $new_lines,
            'instructions' => $new_lines === 'wpautop' ? '1 line break will start a new line, 2 line breaks start a new paragraph' : ( $new_lines === 'br' ? 'Linebreaks will be saved' : '' ),
        ], $args ) );

        return $field;
    }

    public static function content( $field_name = 'content', $field_label = 'Content', $args = [] ) {
        $field = new FieldsBuilder( sanitize_title_with_dashes( $field_name ) );
        $field->addWysiwyg( $field_name, array_merge( [
            'label' => $field_label,
        ], $args ) );

        return $field;
    }

    public static function link( $field_name = 'link', $field_label = 'Link', $return_format = 'array', $args = [] ) {
        $field = new FieldsBuilder( sanitize_title_with_dashes( $field_name ) );
        $field
            ->addLink( $field_name, array_merge( [
                'label'         => $field_label,
                'return_format' => $return_format,
            ], $args ) );

        return $field;
    }

    public static function image( $field_name = 'image_id', $field_label = 'Image', $custom_instructions = '', $minimum_size = 1024, $imageType = 'jpg/png/svg', $args = [] ) {
        $field = new FieldsBuilder( sanitize_title_with_dashes( $field_name ) );
        $field
            ->addImage( $field_name, array_merge( [
                'label'         => $field_label,
                'instructions'  => Base::getImageInstructions( $custom_instructions, $minimum_size, $imageType ),
                'preview_size'  => 'medium',
                'return_format' => 'id',
            ], $args ) );

        return $field;
    }

    public static function gravityForm( $fieldName = 'form', $fieldLabel = 'Form', $args = [] ) {
        $field = new FieldsBuilder( sanitize_title_with_dashes( $fieldName ) );

        $field
            ->addField( $fieldName, 'gravity_forms_field', array_merge( [
                'label' => $fieldLabel,
            ] ) );

        return $field;
    }

	public static function buttonWithGtmTracking( $fieldName = 'button' ) {
		$field = new FieldsBuilder( sanitize_title_with_dashes( 'content_block_settings' ) );
		$field
			->addGroup( $fieldName )
				->addLink( 'link', [
					'wrapper' => [
						'width' => '50'
					]
				] )
				->addText( 'gtm_button_id', [
					'label'   => 'GTM Button ID',
					'instructions' => 'This will be added to  <code>data-gtm-button-id</code>.',
					'wrapper' => [
						'width' => '50'
					]
				] )
			->endGroup();

		return $field;
	}
}


