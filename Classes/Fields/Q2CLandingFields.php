<?php

namespace Q2C_Calculator\Fields;

class Q2CLandingFields extends Base {
	public static function make() {

		$group = self::setupGroup( 'Page options', 1, [
			'hide_on_screen' => [ 'the_content' ]
		] );

		$group
			->addFields( Modules::title() )
			->addUrl( 'video_url', [
				'instructions' => 'Insert a YouTube video url.'
			] )
			->addFields( Modules::image( 'thumbnail_id', 'Video Thumbnail' ) )
			->addFields( Modules::textarea( 'introduction', 'Introduction' ) )
			->addGroup( 'highlighted_text' )
				->addFields( Modules::title() )
				->addFields( Modules::textarea() )
			->endGroup()
			->addRepeater( 'usps', [ 'label' => 'USPs' ] )
				->addFields( Modules::text() )
			->endRepeater()

			// Location
			->setLocation( 'page_template', '==', 'q2c-landingpage.php' );

		self::createGroup( $group );
	}
}