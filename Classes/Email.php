<?php

namespace Q2C_Calculator;

class Email {

	public static function init() {
		add_action( 'wp_ajax_q2c_send_email', [ get_called_class(), 'sendEmailCallback' ] );
		add_action( 'wp_ajax_nopriv_q2c_send_email', [ get_called_class(), 'sendEmailCallback' ] );
	}

	public static function sendEmailCallback() {
		if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'q2c_send_email_nonce' ) ) {
			exit();
		}

		$result['type'] = 'success';

		if ( ! empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ) {
			self::sendEmail( $_REQUEST['post_id'] );

			$result = json_encode( $result );
			echo $result;
		} else {
			header( "Location: " . $_SERVER["HTTP_REFERER"] );
		}

		die();
	}

	public static function sendEmail( $post_id ) {
		add_filter( 'wp_mail_content_type', [ get_called_class(), 'setHtmlContentType' ] );
		add_filter( 'wp_mail_from', [ get_called_class(), 'setSenderEmail' ] );
		add_filter( 'wp_mail_from_name', [ get_called_class(), 'setSenderName' ] );

		$data = ViewObjects::getEmailObject( $post_id );

		$subject = $data['first_name'] . ', here\'s your Quote-to-Cart Calculator Results';

		ob_start();
		?>

        <!DOCTYPE html>
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <meta name="x-apple-disable-message-reformatting">
            <!--[if !mso]><!-->
            <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
            <!--<![endif]-->
            <title></title>
            <!--[if mso]>
            <style type="text/css">
            table {
                border-collapse: collapse;
                border-spacing: 0;
            }

            div, td {
                padding: 0;
            }

            div {
                margin: 0 !important;
            }
            </style>
            <noscript>
            <xml>
                <o:OfficeDocumentSettings>
                    <o:PixelsPerInch>96</o:PixelsPerInch>
                </o:OfficeDocumentSettings>
            </xml>
            </noscript>
            <![endif]-->
            <style type="text/css">
                @media screen and (max-width: 350px) {
                    .three-col .column {
                        max-width: 100% !important;
                    }
                }

                @media screen and (min-width: 351px) and (max-width: 460px) {
                    .three-col .column {
                        max-width: 50% !important;
                    }
                }

                @media screen and (min-width: 461px) {
                    .three-col .column {
                        max-width: 33.3% !important;
                    }
                }
            </style>
        </head>
        <body style="background-color:#F6F6F6">

        <div class="three-col"
             style="font-size:0;text-align:center;max-width: 600px;margin: 0 auto;font-family: Helvetica Neue,sans-serif;">
            <div style="padding: 0;">
                <table role="presentation"
                       style="max-width:600px;margin: 0 auto;text-align: left;font-family: Helvetica Neue,sans-serif; font-style: normal; font-weight: normal; font-size: 16px; line-height: 24px; color: #777779;">
                    <tr>
                        <td style="padding: 40px 0;text-align: center;">
                            <img style="width: 154px;height: auto;"
                                 src="<?php echo Utilities::assets_file_path( 'images/email-logo.png' ) ?>"
                                 alt="Cleverbridge logo"
                                 width="154"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img style="width: 100%;height: auto;"
                                 src="<?php echo Utilities::assets_file_path( 'images/email-header.jpg' ) ?>"
                                 alt="Cleverbridge header"/>
                        </td>
                    </tr>
                    <tr style="background-color: white;">
                        <td style="padding: 30px;">
                            <h1 style="font-weight: 300; font-size: 32px; line-height: 38px; text-align: center;">
                                Quote-to-Cart Calculator Results for <?php echo $data['company_name'] ?>
                            </h1>
                            <p>
                                Hi <?php echo $data['first_name'] ?>,<br/><br/>
                                Thank you for taking the time to explore what our Quote-To-Cart solution can do for
                                you.<br/><br/>
                                You can download a copy of your results below.
                            </p>
                            <p style="text-align: center;padding:10px 0 20px 0;font-family: Helvetica Neue,sans-serif">

                                <a href="<?php echo $data['download_pdf_url']; ?>" rel="noopener noreferrer"
                                   style="letter-spacing:1px;display:inline-block;font-weight: bold; font-size: 15px; line-height: 18px; color: #FFFFFF;background: #FF8A00; border-radius: 5px;margin: 0 auto;text-decoration: none;border: 15px solid #FF8A00;">
                                    &nbsp;&nbsp;&nbsp;DOWNLOAD REPORT&nbsp;&nbsp;&nbsp;
                                </a>

                            </p>
                            <p>
                                Quote-to-Cart is a powerful tool that helps your sales reps cut down on manual work,
                                maximize
                                efficiency, and spend more time selling.<br/><br/>
                                Would you like to discuss your results with one of our payment automation experts? If
                                so, you
                                can <a style="color:#FA8049;text-decoration: underline;" rel="noopener noreferrer"
                                       href="https://www.cleverbridge.com/corporate/contact-sales/">schedule a call with
                                    us
                                    here</a>.<br/><br/>
                                Best,<br/>
                                cleverbridge
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div style="width: 100%;height: 30px">&nbsp;</div>

        <div class="three-col"
             style="font-size:0;text-align:center;max-width: 600px;margin: 0 auto;font-family: Helvetica Neue,sans-serif;">
            <div style="background: white;padding: 0 30px 30px;">
                <table role="presentation" width="100%"
                       style="width:100%;max-width: 600px;margin: 0 auto;border-collapse: collapse;">
                    <tr>
                        <td style="padding-top:15px;width: 100%;text-align: center;background: white;">
                            <h2 style="font-family: Helvetica Neue,sans-serif;color: #FF8A00; font-weight: 300; font-size: 28px; line-height: 32px;">
                                Our Services</h2>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="three-col"
             style="font-size:0;text-align:center;max-width: 600px;margin: 0 auto;font-family: Helvetica Neue,sans-serif;">
            <div style="background: white;padding: 0 30px 30px;">
                <!--[if mso]>
                <table role="presentation" width="100%"
                       style="max-width:600px;background: white;border-collapse: collapse;">
                <tr style="padding-top:30px;">
                    <td style="width:200px;padding:20px;" valign="top">
                <![endif]-->

                <div class="column" style="width:100%;max-width:200px;display:inline-block;vertical-align:top;">
                    <div style="padding:10px 10px 0;text-align:center;">
                        <a href="https://www.cleverbridge.com/corporate/what-we-do/manage/#subscription-management"
                           rel="noopener noreferrer"
                           style="display:block;margin-top:0;margin-bottom:10px;"><img
                                    src="<?php echo Utilities::assets_file_path( 'images/subscription-management.jpg' ) ?>"
                                    width="130" alt="Subscription Management"
                                    style="width:100%;height:auto;"/></a>
                    </div>
                </div>
                <!--[if mso]>
                </td>
                <td style="width:200px;padding:20px;" valign="top">
                <![endif]-->
                <div class="column" style="width:100%;max-width:200px;display:inline-block;vertical-align:top;">
                    <div style="padding:10px 10px 0;text-align:center;">
                        <a href="https://www.cleverbridge.com/corporate/what-we-do/optimize/#reporting-analytics"
                           rel="noopener noreferrer"
                           style="display:block;margin-top:0;margin-bottom:10px;"><img
                                    src="<?php echo Utilities::assets_file_path( 'images/reporting-analytics.jpg' ) ?>"
                                    width="130" alt="Reporting & Analytics"
                                    style="width:100%;height:auto;"/></a>
                    </div>
                </div>
                <!--[if mso]>
                </td>
                <td style="width:200px;padding:20px;" valign="top">
                <![endif]-->
                <div class="column" style="width:100%;max-width:200px;display:inline-block;vertical-align:top;">
                    <div style="padding:10px 10px 0;text-align:center;">
                        <a href="https://www.cleverbridge.com/corporate/what-we-do/optimize/#localization"
                           rel="noopener noreferrer"
                           style="display:block;margin-top:0;margin-bottom:10px;"><img
                                    src="<?php echo Utilities::assets_file_path( 'images/localization.jpg' ) ?>"
                                    width="130" alt="Localization"
                                    style="width:100%;height:auto;"/></a>
                    </div>
                </div>
                <!--[if mso]>
                </td>
                </tr>
                <tr>
                <td style="width:200px;padding:20px;" valign="top">
                <![endif]-->
                <div class="column" style="width:100%;max-width:200px;display:inline-block;vertical-align:top;">
                    <div style="padding:10px 10px 0;text-align:center;">
                        <a href="https://www.cleverbridge.com/corporate/what-we-do/manage/#customer-self-service"
                           rel="noopener noreferrer"
                           style="display:block;margin-top:0;margin-bottom:10px;"><img
                                    src="<?php echo Utilities::assets_file_path( 'images/customer-self-service.jpg' ) ?>"
                                    width="130" alt="Customer Self-Service"
                                    style="width:100%;height:auto;"/></a>
                    </div>
                </div>
                <!--[if mso]>
                </td>
                <td style="width:200px;padding:20px;" valign="top">
                <![endif]-->
                <div class="column" style="width:100%;max-width:200px;display:inline-block;vertical-align:top;">
                    <div style="padding:10px 10px 0;text-align:center;">
                        <a href="https://www.cleverbridge.com/corporate/what-we-do/monetize/#billing-invoicing"
                           rel="noopener noreferrer"
                           style="display:block;margin-top:0;margin-bottom:10px;"><img
                                    src="<?php echo Utilities::assets_file_path( 'images/billing-invoicing.jpg' ) ?>"
                                    width="130" alt="Billing & Invoicing"
                                    style="width:100%;height:auto;"/></a>
                    </div>
                </div>
                <!--[if mso]>
                </td>
                <td style="width:200px;padding:20px;" valign="top">
                <![endif]-->
                <div class="column" style="width:100%;max-width:200px;display:inline-block;vertical-align:top;">
                    <div style="padding:10px 10px 0;text-align:center;">
                        <a href="https://www.cleverbridge.com/corporate/what-we-do/monetize/#payment-processing"
                           rel="noopener noreferrer"
                           style="display:block;margin-top:0;margin-bottom:10px;"><img
                                    src="<?php echo Utilities::assets_file_path( 'images/payment-processing.jpg' ) ?>"
                                    width="130" alt="Payment Processing"
                                    style="width:100%;height:auto;"/></a>
                    </div>
                </div>
                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->

            </div>
        </div>

        <div class="three-col"
             style="font-size:0;text-align:center;max-width: 600px;margin: 0 auto;font-family: Helvetica Neue,sans-serif;">
            <div style="padding: 0;">
                <table role="presentation" style="max-width: 600px;margin: 0 auto;" width="100%">
                    <tr>
                        <td style="padding-top: 25px;text-align: center;">
                            <img style="width: 110px; height: auto;"
                                 width="110"
                                 src="<?php echo Utilities::assets_file_path( 'images/email-logo.png' ) ?>"
                                 alt="Cleverbridge logo"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px; line-height: 140%; text-align: center; color: #33385D;font-family: Helvetica Neue,sans-serif;">
                            <p>Copyright © cleverbridge <?php echo date( 'Y' ) ?>. All rights reserved.</p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        </body>
        </html>

		<?php
		$message = ob_get_contents();
		ob_end_clean();
		
		wp_mail( $data['email_address'], $subject, $message );

		remove_filter( 'wp_mail_content_type', [ get_called_class(), 'setHtmlContentType' ] );
		remove_filter( 'wp_mail_from', [ get_called_class(), 'setSenderEmail' ] );
		remove_filter( 'wp_mail_from_name', [ get_called_class(), 'setSenderName' ] );
	}

	public static function setHtmlContentType() {
		return 'text/html';
	}

	function setSenderEmail( $original_email_address ) {
		return 'sales@cleverbridge.com';
	}

	function setSenderName( $original_email_from ) {
		return 'cleverbridge';
	}
}