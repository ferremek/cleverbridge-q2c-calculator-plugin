<?php


namespace Q2C_Calculator;


class Utilities {
	public static function assets_file_path( $filename ) {
		$path = Q2C_DIRECTORY_URL . '/assets/dist/';

		if ( is_admin() ) {
			return $path . $filename;
		}

		return $path . $filename;
	}
}