<?php

namespace Q2C_Calculator;

class Base {
	public static $textdomain = 'q2c_calculator';

	public static function init() {
		load_theme_textdomain( self::$textdomain, get_template_directory() . '/q2c-calculator/languages/' );

		add_action( 'wp_enqueue_scripts', [ get_called_class(), 'add_scripts' ] );
		add_action( 'wp_enqueue_scripts', [ get_called_class(), 'add_styles' ] );

		add_filter( 'wp_check_filetype_and_ext', [ get_called_class(), 'add_svg' ], 10, 4 );
		add_action( 'admin_head', [ get_called_class(), 'fix_svg' ], - 99 );
		add_filter( 'upload_mimes', [ get_called_class(), 'cc_mime_types' ] );

		add_action( 'admin_post_q2c_form_response', [ get_called_class(), 'form_response' ] );
		add_action( 'admin_post_nopriv_q2c_form_response', [ get_called_class(), 'form_response' ] );
		add_action( 'admin_post_q2c_calculate_again', [ get_called_class(), 'calculate_again' ] );
		add_action( 'admin_post_nopriv_q2c_calculate_again', [ get_called_class(), 'calculate_again' ] );

		add_action( 'init', [ get_called_class(), 'add_result_options_page' ] );
		add_filter( 'single_template', [ get_called_class(), 'set_single_q2c_result_template' ] );
		add_filter( 'use_block_editor_for_post_type', [
			get_called_class(),
			'remove_gutenberg_from_template_q2c_landingpage'
		], 10, 2 );
		add_filter( 'wp_headers', [ get_called_class(), 'add_header_noindex_q2c_result' ] );

		add_shortcode( 'q2c-calculator', [ get_called_class(), 'shortcode_callback' ] );

		add_action( 'init', [ 'Q2C_Calculator\PageTemplate', 'get_instance' ] );
	}

	public static function add_q2c_landingpage_template( $templates ) {
		$templates[ Q2C_DIRECTORY_PATH . 'templates/q2c-landingpage.php' ] = 'Q2C Landingpage';

		return $templates;
	}

	public static function add_header_noindex_q2c_result() {
		if ( is_singular( 'q2c-result' ) ) {
			echo "<meta name='robots' content='noindex, nofollow' />";
		}
	}

	public static function set_single_q2c_result_template( $single ) {
		global $post;

		if ( $post->post_type == 'q2c-result' ) {
			if ( file_exists( Q2C_DIRECTORY_PATH . 'templates/q2c-result.php' ) ) {
				return Q2C_DIRECTORY_PATH . 'templates/q2c-result.php';
			}
		}

		return $single;
	}

	public static function form_response() {
		date_default_timezone_set( get_option( 'timezone_string' ) );

		if ( isset( $_POST['q2c_form_nonce'] ) && wp_verify_nonce( $_POST['q2c_form_nonce'], 'q2c_form_nonce' ) ) {
			$data   = [];
			$fields = [
				'average_monthly_revenue',
				'transactions_per_month',
				'average_order_value',
				'renewal_percentage',
				'average_rep_salary',
				'inside_sales_reps',
				'minutes_per_quote',
				'first_name',
				'last_name',
				'email_address',
				'company_name',
				'job_role',
				'newsletter_subscribe'
			];

			foreach ( $fields as $field ) {
				$data[ $field ] = sanitize_text_field( array_key_exists( $field, $_POST ) ? $_POST[ $field ] : '' );
			}

			$title = $data['company_name'] . ' - ' . $data['first_name'] . ' ' . $data['last_name'] . ' - ' . current_time( 'Y-m-d H:i:s' );

			$args = [
				'post_title'  => $title,
				'post_name'   => wp_hash( $title ),
				'post_date'   => current_time( 'Y-m-d H:i:s' ),
				'post_status' => 'publish',
				'post_type'   => 'q2c-result',
			];

			$post_id = wp_insert_post( $args );

			foreach ( $data as $key => $value ) {
				add_post_meta( $post_id, $key, $value, true );
			}

			wp_redirect( get_permalink( $post_id ) );
		} else {
			wp_die( 'Invalid nonce specified' );
		}
	}

	public static function calculate_again() {
		if ( isset( $_POST['q2c_calculate_again'] ) && wp_verify_nonce( $_POST['q2c_calculate_again'], 'q2c_calculate_again' ) ) {
			$post_id = sanitize_text_field( $_POST['post_id'] );

			$fields = [
				'average_monthly_revenue',
				'transactions_per_month',
				'average_order_value',
				'renewal_percentage',
				'average_rep_salary',
				'inside_sales_reps',
				'minutes_per_quote',
			];

			foreach ( $fields as $field ) {
				update_post_meta( $post_id, $field, sanitize_text_field( $_POST[ $field ] ) );
			}

			wp_redirect( get_permalink( $post_id ) );
		} else {
			wp_die( 'Invalid nonce specified' );
		}
	}

	public static function shortcode_callback() {
		return Mustache::mustache_render_template( 'calculator', ViewObjects::getQ2CCalculatorObject() );
	}

	public static function add_scripts() {
		wp_register_script( 'q2c-calculator-scripts', Utilities::assets_file_path( 'js/q2c-calculator-scripts.js' ), [
			'jquery',
			'jquery-migrate'
		], null, true );

		wp_localize_script( 'q2c-calculator-scripts', 'q2c_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script( 'q2c-calculator-scripts' );
	}

	public static function add_styles() {
		wp_enqueue_style( 'q2c-calculator-styles', Utilities::assets_file_path( 'stylesheets/q2c-calculator-styles.css' ), [], null );
	}

	public static function remove_gutenberg_from_template_q2c_landingpage( $can_edit, $post ) {
		if ( get_page_template_slug( get_the_ID() ) === 'q2c-landingpage.php' ) {
			return false;
		}

		return true;
	}

	public static function add_result_options_page() {
		if ( function_exists( 'acf_add_options_page' ) ) {
			acf_add_options_page( [
				'page_title'  => _x( 'Q2C Result Options', 'ACF Option pages', Base::$textdomain ),
				'menu_title'  => _x( 'Q2C Result Options', 'ACF Option pages', Base::$textdomain ),
				'menu_slug'   => 'q2c-result-page-options',
				'capability'  => 'edit_posts',
				'redirect'    => false,
				'parent_slug' => 'edit.php?post_type=q2c-result'
			] );
		}
	}

	public static function add_svg( $data, $file, $filename, $mimes ) {
		global $wp_version;

		if ( $wp_version == '4.7' || ( (float) $wp_version < 4.7 ) ) {
			return $data;
		}

		$filetype = wp_check_filetype( $filename, $mimes );

		return [
			'ext'             => $filetype['ext'],
			'type'            => $filetype['type'],
			'proper_filename' => $data['proper_filename'],
		];
	}

	public static function cc_mime_types( $mimes ) {
		$mimes['svg'] = 'image/svg+xml';

		return $mimes;
	}

	public static function fix_svg() {
		echo '<style>.attachment-266×266, .thumbnail img { width: 100% !important; height: auto !important; } </style>';
	}
}