<?php

namespace Q2C_Calculator;

class Mustache {
    private static $mustache_engine = null;

    public static function get_template( $name ) {
	    $file_name = Q2C_DIRECTORY_PATH . 'views/' . $name . '.mustache';

	    if ( file_exists( $file_name ) ) {
            return file_get_contents( $file_name );
        } else {
            error_log( 'Mustache error: Template ' . $file_name . ' does not exist.' );

            return '';
        }
    }

    public static function mustache_engine() {
        if ( ! is_null( self::$mustache_engine ) ) {
            return self::$mustache_engine;
        }

        return self::$mustache_engine = new \Mustache_Engine;
    }

	public static function mustache_render_template( $template_name, $view_object = [] ) {
		return self::mustache_engine()->render( self::get_template( $template_name ), $view_object );
	}
}
