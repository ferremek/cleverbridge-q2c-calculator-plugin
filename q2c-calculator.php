<?php
/**
 * Cleverbridge Q2C Calculator
 *
 * @package     Cleverbridge Q2C Calculator
 * @author      zengrowth
 * @copyright   2021 zengrowth
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Cleverbridge Q2C Calculator
 * Plugin URI:
 * Description: Cleverbridge Q2C Calculator
 * Version:     1.2
 * Author:      Zengrowth
 * Author URI:  https://zengrowth.de
 * Text Domain: q2c_calculator
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 *
 */

// Block direct access to file
defined( 'ABSPATH' ) or die( 'Not Authorized!' );

// Plugin Defines
define( "Q2C_FILE", __FILE__ );
define( "Q2C_DIRECTORY", dirname( __FILE__ ) );
define( "Q2C_DIRECTORY_BASENAME", plugin_basename( Q2C_FILE ) );
define( "Q2C_DIRECTORY_PATH", plugin_dir_path( Q2C_FILE ) );
define( "Q2C_DIRECTORY_URL", plugins_url( null, Q2C_FILE ) );

require_once( Q2C_DIRECTORY . '/vendor/autoload.php' );

class Q2C_Calculator_Plugin {

	public function __construct() {
		// Plugin uninstall hook
		register_uninstall_hook( Q2C_FILE, array( 'Q2C_Calculator_Plugin', 'plugin_uninstall' ) );

		// Plugin activation/deactivation hooks
		register_activation_hook( Q2C_FILE, array( $this, 'plugin_activate' ) );
		register_deactivation_hook( Q2C_FILE, array( $this, 'plugin_deactivate' ) );

		// Plugin Actions
		add_action( 'plugins_loaded', array( $this, 'plugin_init' ) );
	}

	/**
	 * Plugin uninstall function
	 * called when the plugin is uninstalled
	 * @method plugin_uninstall
	 */
	public static function plugin_uninstall() {
		//
	}

	/**
	 * Plugin activation function
	 * called when the plugin is activated
	 * @method plugin_activate
	 */
	public function plugin_activate() {
		if ( ! is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) and current_user_can( 'activate_plugins' ) ) {
			wp_die( 'Sorry, but this plugin requires <a href="https://www.advancedcustomfields.com/pro/" target="_blank">Advanced Custom Fields Pro</a> to be installed and active. <br><br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Return to Plugins</a>' );
		}
	}

	/**
	 * Plugin deactivate function
	 * is called during plugin deactivation
	 * @method plugin_deactivate
	 */
	public function plugin_deactivate() {
		//
	}

	/**
	 * Plugin init function
	 * init the polugin textDomain
	 * @method plugin_init
	 */
	function plugin_init() {
		Q2C_Calculator\Base::init();
		Q2C_Calculator\PostTypes::init();
		Q2C_Calculator\Pdf::init();
		Q2C_Calculator\Email::init();
		Q2C_Calculator\Fields\Q2CResultFields::make();
		Q2C_Calculator\Fields\Q2CLandingFields::make();
		Q2C_Calculator\Fields\Q2CResultPageFields::make();
	}

}

new Q2C_Calculator_Plugin();