module.exports = form = function () {
    let $;

    const initQ2CForm = () => {
        const form = $('.q2c-form')
        const next = form.find('[data-q2c-form-next-step]')
        const previous = form.find('[data-q2c-form-previous-step]')
        const formIsMultistep = form.hasClass('q2c-form--is-multistep')

        let currentStep = 1;

        next.click(e => {
            e.preventDefault()

            if (validateStep(currentStep)) {
                currentStep++
                showStep(currentStep)
            }
        });

        previous.click(e => {
            e.preventDefault()

            currentStep--;

            showStep(currentStep)
        });

        form.submit(e => {
            if (formIsMultistep) {
                if (validateStep(currentStep) === false) {
                    e.preventDefault()
                }
            } else {
                if (validateInputs(form) === false) {
                    e.preventDefault()
                }
            }
        });

        $('[data-input-explanation-trigger]').click(function () {
            $(this).parent().toggleClass('q2c-form__explanation--is-open')
        });

        $('[data-input-explanation-close]').click(function () {
            $(this).parent().parent().removeClass('q2c-form__explanation--is-open')
        });
    }

    const showStep = i => {
        const steps = $('.q2c-form__step');

        steps.hide()

        $(`[data-q2c-form-step="${i}"]`).show()
    }

    const validateStep = i => {
        const step = $(`[data-q2c-form-step="${i}"]`)

        return validateInputs(step)
    }

    const validateInputs = parent => {
        parent = $(parent)

        const inputs = parent.find('input, select')
        let noErrors = true;

        inputs.each(function () {
            if (validateInput(this) === false) {
                noErrors = false;
            }
        })

        return noErrors
    }

    const validateInput = e => {
        const i = $(e);
        const p = i.parent().parent('.q2c-form__group');
        const bannedEmailDomains = ['gmail.', 'hotmail.', 'gmx.', 'yahoo.'];

        p.removeClass('q2c-form__group--has-error');

        // don't allow personal email addresses
        if (i.attr('type') === 'email' && i.val()) {
            for (let x = 0; x < bannedEmailDomains.length; x++) {
                let v = i.val()
                let d = bannedEmailDomains[x]

                console.log(d)

                if (v.indexOf(d) >= 0) {
                    p.find('.q2c-form__error').text('Please enter your business email address')
                    p.addClass('q2c-form__group--has-error')

                    return false
                }
            }

            return true;
        }

        if (!i.val()) {
            p.addClass('q2c-form__group--has-error')

            return false;
        }

        return true;
    }

    const initQ2cInputRange = () => {
        const form = $('.q2c-form');
        const range = form.find('input[type=range]')

        range.on('input', function () {
            const v = $(this).val();
            const span = $('[data-range-value]')

            span.text(v + '%');
        });
    }

    const preventFormSubmitWithEnterPress = () => {
        $(window).keydown(function (event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                return false;
            }
        });
    }

    const calculateAndPrefillInputs = () => {
        const form = $('.q2c-form');
        const averageMonthlyRevenue = form.find('input#average_monthly_revenue');
        const transactionsPerMonth = form.find('input#transactions_per_month');
        const averageOrderValue = form.find('input#average_order_value');

        averageMonthlyRevenue.change(() => {
            if (transactionsPerMonth.val()) {
                averageOrderValue.val(
                    Math.round(averageMonthlyRevenue.val() / transactionsPerMonth.val())
                )
            } else if (averageOrderValue.val()) {
                transactionsPerMonth.val(
                    Math.round(averageMonthlyRevenue.val() / averageOrderValue.val())
                )
            }
        })

        transactionsPerMonth.change(() => {
            if (averageMonthlyRevenue.val()) {
                averageOrderValue.val(
                    Math.round(averageMonthlyRevenue.val() / transactionsPerMonth.val())
                )
            } else if (averageOrderValue.val()) {
                averageMonthlyRevenue.val(
                    Math.round(transactionsPerMonth.val() * averageOrderValue.val())
                )
            }
        })

        averageOrderValue.change(() => {
            if (averageMonthlyRevenue.val()) {
                transactionsPerMonth.val(
                    Math.round(averageMonthlyRevenue.val() / averageOrderValue.val())
                )
            } else if (transactionsPerMonth.val()) {
                averageMonthlyRevenue.val(
                    Math.round(transactionsPerMonth.val() * averageOrderValue.val())
                )
            }
        })
    }

    const initConsentCheckbox = function () {
        $('.q2c-form__group--consent input:checkbox').click(function () {
            $(this).attr('checked', !!this.checked)
            $(this).attr('value', this.checked ? 1 : 0)
        });
    }

    return {
        init: function () {
            $ = jQuery

            initQ2CForm();
            initQ2cInputRange();
            calculateAndPrefillInputs();
            preventFormSubmitWithEnterPress();
            initConsentCheckbox();
        },
    };
}();
