module.exports = resultPage = function () {
    let $;

    const initQ2CResultPage = function () {
        const modal = $('[data-q2c-calculate-again-modal]');

        $('[data-calculate-again-button]').click(function () {
            modal.toggleClass('q2c-calculate-again--visible')
        });

        $('[data-calculate-again-close]').click(function () {
            modal.toggleClass('q2c-calculate-again--visible')
        });

        $('[data-request-email]').click(e => {
            e.preventDefault()

            const t = $(this);
            const nonce = $('.q2c-result').data('nonce');
            const post_id = $('.q2c-result').data('post-id');

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: q2c_ajax.ajax_url,
                data: {
                    action: 'q2c_send_email',
                    nonce: nonce,
                    post_id: post_id,
                },
                success: function (response) {
                    $('[data-request-email]').hide()
                    if (response.type === 'success') {
                        $('.q2c-result__request-email__success').show()
                    } else {
                        $('.q2c-result__request-email__fail').show()
                    }
                }
            });
        });
    }

    return {
        init: function () {
            $ = jQuery;

            initQ2CResultPage();
        },
    };
}();
