const fancybox = require('../vendor/fancybox/dist/jquery.fancybox.js');

const form = require('./components/form')
      resultPage = require('./components/result-page');

(function () {
    let $;

    return {
        init: function () {
            $ = jQuery;

            form.init()
            resultPage.init()
        }
    };
})().init();