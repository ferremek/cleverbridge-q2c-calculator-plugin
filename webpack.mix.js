let mix = require( 'laravel-mix' );
require( 'laravel-mix-imagemin' );
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
.setPublicPath('./')
.js( 'assets/src/js/q2c-calculator-scripts.js', 'assets/dist/js' )
.sass( 'assets/src/sass/q2c-calculator-styles.scss', 'assets/dist/stylesheets' )
.options( {
    processCssUrls: false
} )
.copy( 'assets/src/images', 'assets/dist/images' )
.copyDirectory( 'assets/src/fonts', 'assets/dist/fonts' )
.copyDirectory( 'assets/src/vendor', 'assets/dist/vendor' )
.copyDirectory( 'assets/src/videos', 'assets/dist/videos' )
.copyDirectory( 'assets/src/pdfs', 'assets/dist/pdfs' )
.browserSync( {
    proxy: 'http://cleverbridge-2.test',
    has_ssl: false,
    injectChanges: true,
    logSnippet: true,
    files: [
        'assets/dist/stylesheets/style.css',
        'assets/dist/js/*.js',
        '**/*.+(php|mustache)',
    ]
} );

if ( mix.inProduction() ) {
    mix.version();
    mix.imagemin( 'assets/dist/images/*' );
}
