<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'cf5ad9622adc6c21c0956a5b41efca754ef24c7c',
    'name' => 'ferre/q2c-calculator',
  ),
  'versions' => 
  array (
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'ferre/q2c-calculator' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'cf5ad9622adc6c21c0956a5b41efca754ef24c7c',
    ),
    'mpdf/mpdf' => 
    array (
      'pretty_version' => 'v8.0.4',
      'version' => '8.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd3147a0d790b6d11936fd9c73fa31a7ed45e3f6f',
    ),
    'mustache/mustache' => 
    array (
      'pretty_version' => 'v2.13.0',
      'version' => '2.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e95c5a008c23d3151d59ea72484d4f72049ab7f4',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
      'replaced' => 
      array (
        0 => '1.10.2',
      ),
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.99',
      'version' => '9.99.99.0',
      'aliases' => 
      array (
      ),
      'reference' => '84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'setasign/fpdi' => 
    array (
      'pretty_version' => 'v2.3.5',
      'version' => '2.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f2246c8669bd25834f5c264425eb0e250d7a9312',
    ),
    'stoutlogic/acf-builder' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1fbdb58ad46ac77d6df2549aad130854f5e69cf2',
    ),
  ),
);
