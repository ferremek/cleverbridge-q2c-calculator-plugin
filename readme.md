# Cleverbridge Q2C Calculator

## Development
- Install npm dependencies with `npm install`.
- Edit proxy url in `webpack.mix.js`.
- Start dev server with `npm run watch`.

## Project Notes
- [Mustache](https://mustache.github.io/) is used as templating engine.
- [Laravel Mix](https://laravel.com/docs/8.x/mix) for compiling and bundling of assets.
- [ACF Pro](https://www.advancedcustomfields.com/pro/) used for creating custom fields.
- Composer `vendor` folder is kept in Git on purpose.

-------
_For any development related questions email <ferre@zengrowth.de>._