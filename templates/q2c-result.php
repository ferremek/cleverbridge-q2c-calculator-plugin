<?php
global $post;

get_header();
echo \Q2C_Calculator\Mustache::mustache_render_template( 'result-content', \Q2C_Calculator\ViewObjects::getResultContent( $post ) );
echo \Q2C_Calculator\Mustache::mustache_render_template( 'calculate-again', \Q2C_Calculator\ViewObjects::getCalculateAgainObject( $post ) );
get_footer();
