<?php
global $post;

get_header();
?>

<div class="q2c-landingpage">
    <div class="q2c-header">&nbsp;</div>
    <div class="q2c-landingpage__content">
        <div class="q2c-container">
            <div class="q2c-frame">
                <div class="q2c-bit-xxs-12 q2c-bit-md-6">
                    <h1 class="q2c-landingpage__title"><?php echo get_field( 'title' ) ?></h1>
                    <div class="visible-desktop">
                        <?php echo \Q2C_Calculator\Mustache::mustache_render_template( 'landingpage-content', \Q2C_Calculator\ViewObjects::getLandingpageContent( $post ) ); ?>
                    </div>
                </div>
                <div class="q2c-bit-xxs-12 q2c-bit-md-6">
                    <div class="q2c-card">
                        <div class="q2c-card__content">
                            <?php echo do_shortcode( '[q2c-calculator]' ) ?>
                        </div>
                    </div>
                </div>
                <div class="q2c-bit-xxs-12 hidden-desktop">
                    <?php echo \Q2C_Calculator\Mustache::mustache_render_template( 'landingpage-content', \Q2C_Calculator\ViewObjects::getLandingpageContent( $post ) ); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();